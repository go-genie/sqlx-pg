module gitee.com/go-genie/sqlx-pg

go 1.21

require (
	gitee.com/go-genie/geography v1.0.1
	gitee.com/go-genie/kvcondition v1.0.0
	gitee.com/go-genie/sqlx v1.0.1
	github.com/google/uuid v1.3.0
	github.com/onsi/gomega v1.18.1
	github.com/sirupsen/logrus v1.5.0
)

require (
	gitee.com/go-genie/enumeration v1.0.1 // indirect
	gitee.com/go-genie/logr v1.0.1 // indirect
	gitee.com/go-genie/ptr v1.0.1 // indirect
	gitee.com/go-genie/xx v1.0.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/lib/pq v1.10.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220315194320-039c03cc5b86 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
