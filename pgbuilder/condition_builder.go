package pgbuilder

import (
	"context"
	"gitee.com/go-genie/sqlx"
	"gitee.com/go-genie/sqlx/builder"
)

func ToCondition(db sqlx.DBExecutor, b ConditionBuilder, ctx context.Context) (builder.SqlCondition, error) {
	if b == nil {
		return builder.EmptyCond(), nil
	}
	return b.ToCondition(db, ctx)
}

type ConditionBuilder interface {
	ToCondition(db sqlx.DBExecutor, ctx context.Context) (builder.SqlCondition, error)
}

func ConditionBuilderFromCondition(c builder.SqlCondition) ConditionBuilder {
	return ConditionBuilderBy(
		func(db sqlx.DBExecutor) (builder.SqlCondition, error) {
			return c, nil
		},
	)
}

func ConditionBuilderBy(build func(db sqlx.DBExecutor) (builder.SqlCondition, error)) ConditionBuilder {
	return &conditionBuilder{build: build}
}

type conditionBuilder struct {
	build func(db sqlx.DBExecutor) (builder.SqlCondition, error)
}

func (c *conditionBuilder) ToCondition(db sqlx.DBExecutor, ctx context.Context) (builder.SqlCondition, error) {
	return c.build(db)
}

func OneOf(builders ...ConditionBuilder) ConditionBuilder {
	return &conditionBuilderCompose{
		typ:      "or",
		builders: builders,
	}
}

func AllOf(builders ...ConditionBuilder) ConditionBuilder {
	return &conditionBuilderCompose{
		typ:      "all",
		builders: builders,
	}
}

type conditionBuilderCompose struct {
	typ      string
	builders []ConditionBuilder
}

func (c *conditionBuilderCompose) ToCondition(db sqlx.DBExecutor, ctx context.Context) (builder.SqlCondition, error) {
	where := builder.EmptyCond()

	for i := range c.builders {
		b := c.builders[i]
		if b == nil {
			continue
		}

		sub, err := b.ToCondition(db, ctx)
		if err != nil {
			return nil, err
		}

		if builder.IsNilExpr(sub) {
			continue
		}

		switch c.typ {
		case "or":
			where = where.Or(sub)
		case "all":
			where = where.And(sub)
		}
	}

	return where, nil
}

type SubConditionBuilder interface {
	ConditionBuilder
	SelectFrom(db sqlx.DBExecutor) *StmtSelect
}

func SubSelect(target *builder.Column, subConditionBuilder SubConditionBuilder, ctx context.Context) ConditionBuilder {
	return ConditionBuilderBy(
		func(db sqlx.DBExecutor) (builder.SqlCondition, error) {
			if subConditionBuilder == nil {
				return nil, nil
			}

			where, err := subConditionBuilder.ToCondition(db, ctx)
			if err != nil {
				return nil, err
			}
			if builder.IsNilExpr(where) {
				return nil, nil
			}

			return target.In(subConditionBuilder.SelectFrom(db).Where(where)), nil
		},
	)
}
