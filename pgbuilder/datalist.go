package pgbuilder

import (
	"context"
	"gitee.com/go-genie/sqlx"
	"gitee.com/go-genie/sqlx/builder"
)

type DataList interface {
	sqlx.ScanIterator
	ConditionBuilder
	DoList(db sqlx.DBExecutor, pager *Pager, additions ...builder.Addition) error
}

func BatchDoList(db sqlx.DBExecutor, ctx context.Context, scanners ...DataList) (err error) {
	if len(scanners) == 0 {
		return nil
	}

	for i := range scanners {
		scanner := scanners[i]

		cond, err := scanner.ToCondition(db, ctx)
		if err != nil {
			return err
		}

		if !builder.IsNilExpr(cond) {
			if err := scanner.DoList(db, nil); err != nil {
				return err
			}
		}
	}

	return nil
}
